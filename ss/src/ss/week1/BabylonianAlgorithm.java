package ss.week1;

import ss.utils.TextIO;

public class BabylonianAlgorithm {

	public static void main(String[] args) {
		double n = -1;
		while(n < 0) {
			System.out.println("Pick a postive number");
			n = TextIO.getlnDouble();
		}
		
		double gOrig = n/2;
		double g1 = guess(gOrig, n);
		double g2 = guess(g1, n);
		
		while(g1 - g2 > gOrig/100) {
			g1 = g2;
			g2 = guess(g1, n);
		}

		System.out.println(String.format("%.1f" , g2));
	}
	
	public static double guess(double guess, double n) {
		return (guess + n/guess)/2;
	}

}
