package ss.week1;

import ss.utils.TextIO;

public class GrossAndDozens {

	public static void main(String[] args) {
		System.out.println("How many eggs do you have?");
		int eggs = TextIO.getlnInt();
//		eggs % 144 / 12
		System.out.println("Your number of eggs is " + eggs/144 + " gross , " + (eggs-(eggs/144)*144)/12 + " dozen , and " + eggs%12);
	}

}
