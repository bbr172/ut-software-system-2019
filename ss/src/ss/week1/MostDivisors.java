package ss.week1;

public class MostDivisors {

	public static void main(String[] args) {
		int largest[] = {0, 0};
		for (int i = 1; i < 10000; i++) {
			int count = 0;
			for (int j = 1; j < 10000; j++) {
				if (i%j == 0) {
					count++;
				}
			}
			if (largest[1] < count) {
				largest[0] = i;
				largest[1] = count;
			}
		}
		System.out.println(largest[0]);
	}

}
