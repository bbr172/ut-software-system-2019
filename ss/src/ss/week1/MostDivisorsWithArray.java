package ss.week1;

import java.util.Arrays;

public class MostDivisorsWithArray {

	public static void main(String[] args) {
		int[] largestNumbers = new int[10000];
		int largest = 0;
		for (int i = 1; i < 10000; i++) {
			int count = 0;
			for (int j = 1; j < 10000; j++) {
				if (i%j == 0) {
					count++;
				}
			}
			
			if (largest < count) {
				Arrays.fill(largestNumbers, 0);
				largestNumbers[i] = 1;
				largest = count;
			} else if(largest == count) {
				largestNumbers[i] = 1;
			}
		}
		
		System.out.println("Among integers between 1 and 10000 ,\n" + 
				"The maximum number of divisors was " + largest + "\n" + 
				"Numbers with that many divisors include:");
		for (int i = 0; i < largestNumbers.length; i++) {
			if(largestNumbers[i] == 1) {
				System.out.println(i);
			}
		}
	}

}
