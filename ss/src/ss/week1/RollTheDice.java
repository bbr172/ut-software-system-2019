package ss.week1;

public class RollTheDice {

	public static void main(String[] args) {
		int[] x = {(int) ((Math.random()*6) + 1), (int) ((Math.random()*6) + 1)};
		System.out.println("The first die comes up " + Integer.toString(x[0]) + 
				"\nThe second die comes up " + Integer.toString(x[1]) + 
				"\nYour final result is " + Integer.toString(x[0] + x[1]));
	}

}
