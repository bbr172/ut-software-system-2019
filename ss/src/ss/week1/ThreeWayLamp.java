package ss.week1;

import ss.utils.TextIO;

public class ThreeWayLamp {

	enum Options {OFF, LOW, MEDIUM, HIGH};
	
	public static void main(String[] args) {
		boolean exit = false;
		Options state = Options.OFF;
		help();
		while(!exit) {
			String input = (TextIO.getWord()).toLowerCase();
			switch(input) {
			case "off":
				state = Options.OFF;
				break;
			case "low":
				state = Options.LOW;
				break;
			case "medium":
				state = Options.MEDIUM;
				break;
			case "high":
				state = Options.HIGH;
				break;
			case "state":
				System.out.println(state);
				break;
			case "next":
				state = next(state);
				break;
			case "help":
				help();
				break;
			case "exit":
				exit = true;
				break;
			default: System.out.println("Error");
			}
		}
	}
	
	public static void help() {
		System.out.println("Choose your option:\n [OFF, LOW, MEDIUM, HIGH, STATE, NEXT, HELP, EXIT]");
	}

	public static Options next(Options state) {
		switch (state) {
		case OFF: return Options.LOW;
		case LOW: return Options.MEDIUM;
		case MEDIUM: return Options.HIGH;
		default: return Options.OFF;
		}
	}
}
