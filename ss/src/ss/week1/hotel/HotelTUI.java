package ss.week1.hotel;

import ss.utils.TextIO;

public class HotelTUI {
	public static void main(String[] args) {
		Boolean exit = false;
		String guest[] = {null, null};
		help();

		while(!exit) {
			String input = TextIO.getln();
			String split[] = input.split(" ", 2);
			String command = split[0];
			String parm = null; 
			if (split.length > 1 && split[1].replace(" ", "").length() > 0) {
				parm = split[1];
			}
			
			switch(command) {
			case "i":
				if(parm == null) {
					System.out.println("Missing guest name");
				} else if(guest[0] == null) {
					String room = Integer.toString((int) (Math.random() * 50) + 1);
					System.out.println("Guest " + parm + " gets room " + room);
					guest[0] = parm;
					guest[1] = room;
				} else {
					System.out.println("The hotel is full");
				}
				break;
			case "o": 
				if(parm == null) {
					System.out.println("Missing guest name");
				} else if(guest[0] == null) {
					System.out.println("No guest in hotel");
				} else if(parm.toLowerCase().equals(guest[0].toLowerCase())) {
					System.out.println("Guest " + parm + " has checked out");
					guest[0] = null;
					guest[0] = null;
				} else {
					System.out.println("Guest " + parm + " doesn't have a room");
				}
				break;
			case "r": 
				if(parm == null) {
					System.out.println("Missing guest name");
				} else if(guest[0] == null) {
					System.out.println("No guest in hotel");
				} else if(parm.toLowerCase().equals(guest[0].toLowerCase())) {
					System.out.println("Guest " + parm + " have room " + guest[1]);
				} else {
					System.out.println("Guest " + parm + " doesn't have a room");
				}
				break;
			case "h": 
				help();
				break;
			case "p": 
				if(guest[0] != null) {
					System.out.println("Guest " + guest[0] + " sleeping in room " + guest [1]);
				} else {
					System.out.println("No guest in the hotel");
				}
				break;
			case "x": 
				exit = true;
				break;
			default: 
				System.out.println("Wrong input");
			}
		}
	}
	
	public static void help() {
		System.out.println("Welcome to the Hotel booking system of the U Parkhotel\n"
				+ "Commands:\n"
				+ "i name ............... check in guest with name\n"
				+ "o name ............... check out guest with name\n"
				+ "r name ............... request room of guest\n"
				+ "h .................... help ( this menu )\n"
				+ "p .................... print state\n"
				+ "x .................... exit\n\n");
	}
}
