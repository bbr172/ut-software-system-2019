package ss.week1.hotel;

public class SimpleBillPrinter {

	public static void main(String[] args) {
		String bill;
		String description = "Hotel night 1x";
		Double value = 85.00;
		bill = description + "\t\t" + String.format("%.2f", value);
		System.out.format(bill);
	}

}
