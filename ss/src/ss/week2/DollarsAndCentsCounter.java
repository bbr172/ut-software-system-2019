package ss.week2;

public class DollarsAndCentsCounter {
	
	private int dollars = 0;
	private int cents = 0;

	public int dollars() {
		if (dollars >= 0) {
			return dollars;
		} else {
			return 0;
		}
	}

	public int cents() {
		if (cents >= 0) {
			return cents;
		} else {
			return 0;
		}
	}
	
	public void add(int dollars, int cents) {
		this.dollars = this.dollars + dollars;
		this.cents = this.cents + cents;
	}

	public void reset() {
		dollars = 0;
		cents = 0;
	}
	
}
