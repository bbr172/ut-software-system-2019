package ss.week2;

/**
 * @invariant state != null;
 *
 */
public class ThreeWayLamp {
	
	public enum Option {OFF, LOW, MEDIUM, HIGH};
	
	private Option state;
	
	/** 
	 * Creates the object ThreeWayLamp
	 * 
	 * @requires nothing;
	 * @ensures state = Option.OFF
	 */
	public ThreeWayLamp() {
		setState(Option.OFF);
	}
	
	/** 
	 * Set state of ThreeWayLamp
	 * 
	 * @requires state != null;
	 * @ensures this.state = state
	 */
	public void setState(Option state) {
		assert state != null;
		
		this.state = state;
	}
	
	/** 
	 * Get state of ThreeWayLamp
	 * 
	 * @ensures the return of the state
	 */
	public Option getState() {
		assert this.state != null;
		
		return this.state;
	}
	
	/** 
	 * Set next state of ThreeWayLamp
	 * 
	 * @ensures the lamp has the in next state (OFF->LOW, LOW->MEDIUM, MEDIUM->HIGH, HIGH->LOW)
	 */
	public void next() {
		assert this.state != null;
		
		switch (this.state) {
		case OFF: 
			this.state = Option.LOW;
			break;
		case LOW: 
			this.state = Option.MEDIUM;
			break;
		case MEDIUM: 
			this.state = Option.HIGH;
			break;
		case HIGH: 
			this.state = Option.OFF;
			break;
		}
	}
	
}


