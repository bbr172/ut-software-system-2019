package ss.week2;

import ss.utils.TextIO;
import ss.week2.ThreeWayLamp.Option;

public class ThreeWayLampTUI {

	public static void main(String[] args) {
		ThreeWayLamp twl = new ThreeWayLamp();
		boolean exit = false;
		help();
		
		while(!exit) {
			String input = (TextIO.getWord()).toLowerCase();
			switch(input) {
			case "off":
				twl.setState(Option.OFF);
				break;
			case "low":
				twl.setState(Option.OFF);
				break;
			case "medium":
				twl.setState(Option.OFF);
				break;
			case "high":
				twl.setState(Option.OFF);
				break;
			case "state":
				System.out.println(twl.getState());
				break;
			case "next":
				twl.next();
				break;
			case "help":
				help();
				break;
			case "exit":
				exit = true;
				break;
			default: System.out.println("Error, woring input.");
			}
		}
	}
	
	public static void help() {
		System.out.println("Choose your option:\n [OFF, LOW, MEDIUM, HIGH, STATE, NEXT, HELP, EXIT]");
	}

}
