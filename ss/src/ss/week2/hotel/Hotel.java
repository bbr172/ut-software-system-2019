package ss.week2.hotel;

/**
 * @invariant r101, r102
 *
 */

public class Hotel {

	private String name;
	private Room r101, r102;
	
	/**
	 * 
	 * @requires hotel name
	 * @ensures name of hotel is set, rooms are created 
	 * @param name
	 */
	public Hotel(String name) {
		assert name != null;
		this.name = name;
		r101 = new Room(101);
		r102 = new Room(102);
	}
	
	/**
	 * @requires guestName != null, free room
	 * @ensures guest is checkedin
	 * @param guestName
	 * @return Room or null
	 */
	public Room checkIn(String guestName) {
		assert name != guestName;
		Room r = getFreeRoom();
		if (r == null && getRoom(guestName) == null) {
			return null;
		}
		
		r.setGuest(new Guest(guestName));
		return r;
	}
	
	/**
	 * @requires guest has a room
	 * @ensures guest is checked out
	 * @param guestName
	 */
	public void checkOut(String guestName) {
		Room r = getRoom(guestName);
		if (r != null) {
			r.getSafe().deactivate();
			r.setGuest(null);	
		}
	}
	
	/**
	 * @ensures a free room or no free room.
	 * @return free room or null
	 */
	public Room getFreeRoom() {
		if (r101.getGuest() == null) {
			return r101;
		} else if (r102.getGuest() == null) {
			return r102;
		} else {
			return null;
		}
	}
	
	/**
	 * 
	 * @requires guest name, guest has a room
	 * @ensures the room of a guest
	 * @param guestName
	 * @return
	 */
	public Room getRoom(String guestName) {
		guestName = guestName.toLowerCase();
		if (r101.getGuest() != null) {
			if (r101.getGuest().getName().toLowerCase().equals(guestName)) {
				return r101;
			}
		}
		if (r102.getGuest() != null) {
			if (r102.getGuest().getName().toLowerCase().equals(guestName)) {
				return r101;
			}
		}
		return null;
	}
	
	/**
	 * @ensures if hotel is empty
	 * @return empty of not
	 */
	public boolean empty() {
		if(r101 == null && r102 == null) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @ensures a nice way of showing the object. 
	 */
	public String toString() {
		return "Room 101: " + r101.getGuest() + " safe is: " + r101.getSafe().isActive() + "\n" +
				"Room 102: " + r102.getGuest() + " safe is: " + r102.getSafe().isActive() + "\n";
	}
	
}
