package ss.week2.hotel;

import ss.utils.TextIO;

public class HotelTUI {
	
	private Hotel hotel;
	
	public static void main(String[] args) {
		(new HotelTUI("U_Parkhotel")).start();
		
	}
	
	public HotelTUI(String name) {
		assert name != null;
		hotel = new Hotel(name);
	}
	
	public void start() {
		Boolean exit = false;
		printHelpMenu();
	
		while(!exit) {
			String input = TextIO.getln();
			String split[] = input.split(" ", 2);
			String command = split[0];
			String parm = null; 
			if (split.length > 1 && split[1].replace(" ", "").length() > 0) {
				parm = split[1];
			}
			
			switch(command) {
			case "i":
				if(parm == null) {
					System.out.println("Missing guest name");
				} else if(hotel.getRoom(parm) != null){
					System.out.println("The guest alreay has a room");
				} else if(hotel.getFreeRoom() == null) {
					System.out.println("The hotel is full");
				} else {
					Room r = hotel.checkIn(parm);
					if (r == null) {
						System.out.println("Somingt went wrong");
					} else {
						System.out.println("Guest " + parm + " is assigned to room: " + r.getNumber());
					}
				}
				break;
			case "o": 
				if(parm == null) {
					System.out.println("Missing guest name");
				} else if(hotel.empty()) {
					System.out.println("No guest in hotel");
				} else if(hotel.getRoom(parm) == null) {
					System.out.println("Guest " + parm + " doesn't have a room");
				} else {
					hotel.checkOut(parm);
					System.out.println("Guest " + parm + " has checked out");
				}
				break;
			case "r": 
				if(parm == null) {
					System.out.println("Missing guest name");
				} else if(hotel.empty()) {
					System.out.println("No guest in hotel");
				} else {
					Room r = hotel.getRoom(parm);
					if (r == null) {
						System.out.println("Guest " + parm + " doesn't have a room");
					} else {
						System.out.println("Guest " + parm + " have room " + r.getNumber());
					}
				}
				break;
			case "h": 
				printHelpMenu();
				break;
			case "p": 
				if(hotel.empty()) {
					System.out.println("No guest in hotel"); 
				} else {
					System.out.println(hotel.toString());
				}
				break;
			case "x": 
				exit = true;
				break;
			default: 
				System.out.println("Wrong input");
			}
		}
	}

	
	public void printHelpMenu() {
		System.out.println("Welcome to the Hotel booking system of the U Parkhotel\n"
				+ "Commands:\n"
				+ "i name ............... check in guest with name\n"
				+ "o name ............... check out guest with name\n"
				+ "r name ............... request room of guest\n"
				+ "h .................... printHelpMenu ( this menu )\n"
				+ "p .................... print state\n"
				+ "x .................... exit\n\n");
	}
}
