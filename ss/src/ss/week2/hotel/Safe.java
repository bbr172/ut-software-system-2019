package ss.week2.hotel;

public class Safe {

	private boolean open, active;
	
	public Safe() {
		deactivate();
		close();
	}
	
	public void activate() {
		active = true; 
	}
	
	public void deactivate() {
		active = false;
	}
	
	public void open() {
		if (isActive()) {
			open = true;
		}
	}

	public void close() {
		open = false;
	}
	
	public boolean isActive() {
		if (active) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isOpen() {
		if (this.open) {
			return true;
		} else {
			return false;
		}
	}
}
