package ss.week2.test;

//import ss.week2.*;
import ss.week2.ThreeWayLamp;
import ss.week2.ThreeWayLamp.Option;

public class ThreeWayLampTest {

	private ThreeWayLamp twl;
	
	private void setUp() {
		this.twl = new ThreeWayLamp();
	}
	
	public void runTest() {
		this.setUp();
		if(!this.testCreation()) System.out.println("State is not OFF after creating"); 
		if(!this.testNext()) System.out.println("Error in looping states");
	}
	
	private boolean testCreation() {
		if(twl.getState() == Option.OFF) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean testNext() {
		twl.setState(Option.OFF);
		
		twl.next();
		if(twl.getState() != Option.LOW) return false;

		twl.next();
		if(twl.getState() != Option.MEDIUM) return false;
		
		twl.next();
		if(twl.getState() != Option.HIGH) return false;
		
		twl.next();
		if(twl.getState() != Option.OFF) return false;
		
		return true;
	}
	
	public static void main(String[] args) {
		new ThreeWayLampTest().runTest();
	}
	
}
