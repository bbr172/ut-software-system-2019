package ss.week3.bill;

//import java.math.BigDecimal;
//import java.math.RoundingMode;

public class Bill {

	private Printer printer;
	private double som;
	
	public Bill(Printer printer) {
		assert printer != null;
		this.printer = printer;
		this.som = 0;
	} 
	
	public void addItem(Item item) {
		assert item != null;
		this.printer.printLine(item.toString(), item.getAmount());
		this.som = this.som + item.getAmount();
//		this.som = (new BigDecimal(this.som)).setScale(2, RoundingMode.HALF_DOWN).doubleValue();
	}
	
	public void close() {
		this.printer.printLine("Total: ", this.som);
	}
	
	//JUST FOR WEEK 7
	public Printer getPrinter() {
		return this.printer;
	}
//	
	
	public double getSum() {
		return (this.som >= 0.0) ? this.som : 0.0;
	}
	

	public interface Item {
				
		public double getAmount();
				
	}
}
