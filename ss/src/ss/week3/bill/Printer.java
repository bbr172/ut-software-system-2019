package ss.week3.bill;

public interface Printer {
	
	public default String format(String text, double price) {
		return String.format("%-30s %10.2f", text, price);
	}
	
	public  void printLine(String text, double price); //{
//		System.out.println(this.format(text, price));
//	}
}
