package ss.week3.bill;

public class StringPrinter implements Printer {

	private String result;
		
	public void printLine(String text, double price) {
		String begin;
		if (this.result != null) {
			begin = this.result + "\n";
		} else {
			begin = "";
		}
		this.result = begin + this.format(text, price);
		
//		this.result = ((this.result != null) ? this.result + "\n" : "") + this.format(text, price);
	}
	
	public String getResult() {
		return this.result;
	}
}
