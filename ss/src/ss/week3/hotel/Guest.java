package ss.week3.hotel;


public class Guest {

	private String name;
	private Room room;
	
	/**
	 * 
	 * @param name (name of the guest)
	 * @ensures this.name != null
	 */
	public Guest(String name) {
		this.name = name;	
	}
	
	/**
	 * 
	 * @return this.room;
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * 
	 * @return this.room
	 */
	public Room getRoom() {
		return this.room;
	}
	
	/**
	 * 
	 * @param room (room of guest)
	 * @return true if room != null, else false
	 * @ensures room != null
	 */
	public boolean checkin(Room room) {
		if (this.room != null || room.getGuest() != null) {
			return false;
		} else {
			room.setGuest(this);
			this.room = room;
			return true;
		}
	}
	
	/**
	 * 
	 * @ensures checkout of guest
	 * @return true if room != null, false if room == null
	 */
	public boolean checkout() {
		if (room == null) {
			return false;
		} else {
			this.room.setGuest(null);
			this.room = null;
			return true;	
		}
	}
	
	public String toString() {
		return String.format("Guest %s" , this.name);
	}
}
