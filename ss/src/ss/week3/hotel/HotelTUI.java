package ss.week3.hotel;

import ss.utils.TextIO;
import ss.week3.bill.SysoutPrinter;

public class HotelTUI {
	
	private Hotel hotel;
	
	public static void main(String[] args) {
		(new HotelTUI("U_Parkhotel")).start();
		
	}
	
	public HotelTUI(String name) {
		assert name != null;
		hotel = new Hotel(name);
	}
	
	public void start() {
		Boolean exit = false;
		printHelpMenu();
	
		while(!exit) {
			String input = TextIO.getln();
			String split[] = input.split(" ");
			String command = split[0];
			String parm1 = null;
			String parm2 = null;
			if (split.length > 1 && split[1].replace(" ", "").length() > 0) {
				parm1 = split[1];
			}
			if (split.length > 2 && split[2].replace(" ", "").length() > 0) {
				parm2 = split[2];
			}
						
			switch(command) {
			case "i":
				if(parm1 == null) {
					System.out.println("Missing guest name");
				} else if(hotel.getRoom(parm1) != null){
					System.out.println("The guest alreay has a room");
				} else if(hotel.getFreeRoom() == null) {
					System.out.println("The hotel is full");
				} else {
					Room r = hotel.checkIn(parm1);
					if (r == null) {
						System.out.println("Somingt went wrong");
					} else {
						System.out.println("Guest " + parm1 + " is assigned to room: " + r.getNumber());
					}
				}
				break;
			case "o": 
				if(parm1 == null) {
					System.out.println("Missing guest name");
				} else if(hotel.empty()) {
					System.out.println("No guest in hotel");
				} else if(hotel.getRoom(parm1) == null) {
					System.out.println("Guest " + parm1 + " doesn't have a room");
				} else {
					hotel.checkOut(parm1);
					System.out.println("Guest " + parm1 + " has checked out");
				}
				break;
			case "r": 
				if(parm1 == null) {
					System.out.println("Missing guest name");
				} else if(hotel.empty()) {
					System.out.println("No guest in hotel");
				} else {
					Room r = hotel.getRoom(parm1);
					if (r == null) {
						System.out.println("Guest " + parm1 + " doesn't have a room");
					} else {
						System.out.println("Guest " + parm1 + " have room " + r.getNumber());
					}
				}
				break;
			case "a":
					if(parm1 == null) {
						System.out.println("Missing guest name");
					} else {
						Room r = hotel.getRoom(parm1);
						if(r == null){
							System.out.println("The guest does not have a room");
						} else if (r.getSafe() == null) {
							System.out.println("No safe in room");
						} else if (r.getSafe() instanceof PricedSafe && parm2 == null) {
							System.out.println("Missing password");
						} else if (r.getSafe() instanceof PricedSafe) {
							((PricedSafe) r.getSafe()).activate(parm2);
						} else {
							r.getSafe().activate();
							System.out.println("Safe in " + r.getNumber() + " of guest " + parm1 + " has been activated.");
						}
					}
					break;
			case "b":
				if(parm1 == null) {
					System.out.println("Missing guest name");
				} else if(hotel.getRoom(parm1) == null) {
					System.out.println("Guest " + parm1 + " doesn't have a room");
				} else if (parm2 == null) {
					System.out.println("Missing nights");
				} else {
					try {
						hotel.getBill(parm1, Integer.parseInt(parm2), new SysoutPrinter());
					} catch (NumberFormatException e) {
						System.out.println("Second parameter is not a number");
					}
					
				}
				break;
			case "h": 
				printHelpMenu();
				break;
			case "p": 
				if(hotel.empty()) {
					System.out.println("No guest in hotel"); 
				} else {
					System.out.println(hotel.toString());
				}
				break;
			case "x": 
				exit = true;
				break;
			default: 
				System.out.println("Wrong input");
			}
		}
	}

	
	public void printHelpMenu() {
		System.out.println("Welcome to the Hotel booking system of the U Parkhotel\n"
				+ "Commands:\n"
				+ "i name ............... check in guest with name\n"
				+ "o name ............... check out guest with name\n"
				+ "r name ............... request room of guest\n"
				+ "a name password ...... activate safe, password required for PricedSafe\n"
				+ "b name nights ........ print bill for guest (name) and number of nights\n"
				+ "h .................... printHelpMenu ( this menu )\n"
				+ "p .................... print state\n"
				+ "x .................... exit\n\n");
	}
}
