package ss.week3.hotel;

import ss.week3.bill.Bill.Item;

public class PricedRoom extends Room implements Item {
	
	private double price;
	private PricedSafe safe;
	
	public PricedRoom(int roomNumber, double price, double safeCosts) {
		super(roomNumber);
		this.price = price;
		this.safe = new PricedSafe(safeCosts);
	}
	
	public PricedSafe getSafe() {
		return this.safe;
	}
	
	@Override
	public String toString() {
		return "Night in room: E" + Double.toString(price);
	}
	
	@Override
	public double getAmount() {
		return price;
	}

}
