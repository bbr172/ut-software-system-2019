package ss.week3.hotel;

import ss.week3.bill.Bill.Item;
import ss.week3.password.Password;

public class PricedSafe extends Safe implements Item{

	private double price;
	private Password password;
	
	public PricedSafe(double price) {
		this.price = price;
		this.password = new Password();
	}
	
	public void activate(String pass) {
		if (this.password.testWord(pass)) {
			super.activate();
		}
	}
	
	@Override
	public void activate() {
		System.out.println("WARNING");
	}
		
	@Override
	public void deactivate() {
		super.deactivate();
		super.close();
	}
	
	public void open(String pass) {
		if (this.isActive() && this.password.testWord(pass)) {
			super.open();
		}		
	}
	
	@Override
	public void open() {
		System.out.println("WARNING");
	}
	
	@Override
	public void close() {
		super.close();
	}
	
	public Password getPassword() {
		return this.password;
	}
	
	@Override
	public double getAmount() {
		return this.price;
	}
	
	@Override
	public String toString() {
		return "Safe: E" + Double.toString(this.price);
	}
	
	public static void main(String[] args) {
		PricedSafe ps = new PricedSafe(9.99);
	}

}
