package ss.week3.hotel;

public class Safe {

	private boolean open, active;
	
	public Safe() {
		deactivate();
		close();
	}
	
	public void activate() {
		active = true; 
	}
	
	public void deactivate() {
		active = false;
	}
	
	public void open() {
		if (isActive()) {
			open = true;
		}
	}

	public void close() {
		open = false;
	}
	
	public boolean isActive() {
		return this.active;
	}
	
	public boolean isOpen() {
		return this.open;
	}
}
