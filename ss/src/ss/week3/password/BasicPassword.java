package ss.week3.password;

public class BasicPassword {
	
	public static final String INITIAL = "DEFAULT";
	private String pass; 
	
	public BasicPassword() {
		this.pass = INITIAL;
	}
	
	public boolean acceptable(String suggestion) {
		return suggestion != null 
				&& suggestion.length() > 5
				&& !suggestion.contains(" ");
	}
	
	public boolean setWord(String oldpass, String newpass) {
		if (oldpass != null && newpass != null && this.acceptable(newpass) && this.pass.equals(oldpass)) {
			this.pass = newpass;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean testWord(String test) {
		return test != null
				&& this.pass.equals(test);
	}
}
