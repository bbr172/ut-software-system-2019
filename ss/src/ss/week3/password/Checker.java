package ss.week3.password;

public interface Checker {
	

	/**
	 * 
	 * Checks if String s is a acceptable password
	 * @ensures s is acceptable
	 * @requires s != null && s > 5 && s doesn't contain spaces
	 * @param String s
	 */
	public default boolean acceptable(String s) {
		return s != null 
				&& s.length() > 5
				&& !s.contains(" ");
	}
	
	/**
	 * 
	 * @ensures a acceptable password
	 */
	public String generatePassword();
	
}
