package ss.week3.password;

public class Password {
	
	public static final String INITIAL = "DEFAULT";
	private String pass; 
	
	private Checker checker;
	
	
	public Password() {
		this(new BasisChecker());
	}
	
	public Password(Checker c) {
		if (c == null) {
			c = new BasisChecker();
		}
		this.checker = c;
		this.pass = this.INITIAL;
	}


	public boolean acceptable(String suggestion) {
		return checker.acceptable(suggestion);
	}
	
	public boolean setWord(String oldpass, String newpass) {
		if (oldpass != null && newpass != null && this.acceptable(newpass) && this.pass.equals(oldpass)) {
			this.pass = newpass;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean testWord(String test) {
		return test != null
				&& this.pass.equals(test);
	}
	
	public String getInitPass() {
		return this.INITIAL;
	}
}
