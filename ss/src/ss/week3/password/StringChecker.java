package ss.week3.password;

import java.lang.Character;

public class StringChecker implements Checker{

	@Override
	public boolean acceptable(String s) {
		return Checker.super.acceptable(s) 
				&& Character.isAlphabetic(s.charAt(0)) 
				&& Character.isDigit(s.charAt(s.length() - 1));
	}
	
	@Override
	public String generatePassword() {
		return "Afsalhdfjlasjfsalkdfj1";
	}

}
