package ss.week3.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ss.week3.bill.Bill;
import ss.week3.bill.Bill.Item;
import ss.week3.bill.StringPrinter;

class BillTest {
	
	private Bill b;
	private StringPrinter p;

	@BeforeEach
	void setUp() throws Exception {
		p = new StringPrinter();
		b = new Bill(p);
	}

	@Test
	void testBeginState() {
		assertEquals(0.0, b.getSum());
	}
	
	@Test
	void testAddItems() {
		b.addItem(new ItemTest("Test object 1", 12.2));
		assertEquals(12.2, b.getSum());
		b.addItem(new ItemTest("Test object 2", 13.67));
		assertEquals(25.87, b.getSum(), 2);
		b.close();
		assertEquals("Test object 1                       12.20\n" + 
				"Test object 2                       13.67\n" + 
				"Total:                              25.87", p.getResult());
	}
	
	
	public class ItemTest implements Item {
		
		private String description;
		private double amount;
		
		public ItemTest(String description, double amount) {
			this.description = description;
			this.amount = amount;
		}

		@Override
		public double getAmount() {
			return !(this.amount < 0) ? this.amount : 0.0;
		}
		
		@Override
		public String toString() {
			return this.description;
		}
	}

}
