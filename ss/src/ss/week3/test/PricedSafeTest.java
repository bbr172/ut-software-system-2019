package ss.week3.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ss.week3.bill.Bill;
import ss.week3.hotel.PricedSafe;

public class PricedSafeTest {

    private PricedSafe safe;
    private static final double PRICE = 6.36;
    private static final String PRICE_PATTERN = ".*6[.,]36.*";
    
    public String CORRECT_PASSWORD;
    public String WRONG_PASSWORD;
    

    @BeforeEach
    public void setUp() throws Exception {
        safe = new PricedSafe(PRICE);
        CORRECT_PASSWORD = safe.getPassword().getInitPass();
        WRONG_PASSWORD = CORRECT_PASSWORD + "WRONG";
        assertFalse(safe.isActive());
        assertFalse(safe.isOpen());
    }
    
    @Test
    public void testIsBillItem() throws Exception {
    	assertTrue(safe instanceof Bill.Item, 
    			"safe should be an instance of Bill.Item.");
        assertEquals(PRICE, safe.getAmount(), 0, 
        		"GetAmount should return the price of the safe.");
    }
    
    @Test
    public void testGetAmount() throws Exception {
    	assertEquals(PRICE, this.safe.getAmount(), 2);
//    	assertEquals(this.safe.toString(), (String) PRICE);
    }
    
    @Test
    public void testSafeActivate() throws Exception {
    	safe.deactivate();
    	safe.activate(CORRECT_PASSWORD);
    	assertTrue(safe.isActive());
    	safe.close();
    	assertTrue(!safe.isOpen());
    }
    
    @Test
    public void testSafeDeactivate() throws Exception {
    	safe.deactivate();
    	safe.activate(WRONG_PASSWORD);
    	assertTrue(!safe.isActive());
    	assertTrue(!safe.isOpen());
    }
    
    @Test
    public void testOpenDeactivateCorrectPassword() throws Exception {
    	safe.deactivate();
    	safe.open(CORRECT_PASSWORD);
    	assertTrue(!safe.isOpen());
    	assertTrue(!safe.isActive());
    	
    }
    
    @Test
    public void testOpenDeactivateWrongPassword() throws Exception {
    	safe.deactivate();
    	safe.open(WRONG_PASSWORD);
    	assertTrue(!safe.isOpen());
    	assertTrue(!safe.isActive());
    	
    }
}