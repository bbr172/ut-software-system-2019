package ss.week4;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class MapUtil {
	
    public static <K, V> boolean isOneOnOne(Map<K, V> map) {
    	V[] values = (V[]) new Object[map.size()];
    	K[] keys = (K[]) new Object[map.size()];
    	int count = 0;
    	for (Entry<K, V> entry : map.entrySet()) {
    		for (int i = 0; i < count; i++) {
    			if (values[i].equals(entry.getValue()) || keys[i].equals(entry.getKey())) {
					return false;
				}
    		}
    		values[count] = entry.getValue();
    		keys[count] = entry.getKey();
    		count++;
   		}
    	return true;
    }
    
    public static <K, V> boolean isSurjectiveOnRange(Map<K, V> map, Set<V> range) {
    	return range.stream().allMatch(value -> map.containsValue(value)) && !map.containsValue(null);
    }
    
    public static <K, V> Map<V, Set<K>> inverse(Map<K, V> map) {
    	Map<V, Set<K>> m = new HashMap<V, Set<K>>();
    	for (Entry<K, V> entry : map.entrySet()) {
    		if (m.containsKey(entry.getValue())) {
    			m.get(entry.getValue()).add(entry.getKey());
    		} else {
    			HashSet<K> s = new HashSet<K>();
        		s.add(entry.getKey());
        		m.put(entry.getValue(), s);
    		}
    	}
    	return m;
	}
    
	public static <K, V> Map<V, K> inverseBijection(Map<K, V> map) {
		if (!isOneOnOne(map) || map.containsValue(null)) {
			return null;
		}

		Map<V, K> m = new HashMap<V, K>();
		map.forEach((K, V) -> m.put(V, K));
		
    	return m;
	}
	
	public static <K, V, W> boolean compatible(Map<K, V> f, Map<V, W> g) {
		return f.values().stream().allMatch(value -> g.containsKey(value));
	}
	
	public static <K, V, W> Map<K, W> compose(Map<K, V> f, Map<V, W> g) {
        assert compatible(f, g);
		Map<K, W> m = new HashMap<K, W>();
        
		f.forEach((K, V) -> m.put(K, g.get(V)));
        
        return m;
	}
}
