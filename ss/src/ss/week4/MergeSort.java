package ss.week4;

import java.util.ArrayList;
import java.util.List;

public class MergeSort {
				
    public static <E extends Comparable<E>> void mergesort(List<E> list) {   	
//    	System.out.println("Originale " + list);

    	MergeSort m = new MergeSort();
    	list = m.sort(list);
    	System.out.println("Sorted " + list);

    }
    
    public <E extends Comparable<E>> List<E> sort(List<E> list) {
    	List<E> returnList = new ArrayList<E>();
    	
    	if (list.size() > 2) {

    		List<E> leftList;
        	List<E> rightList;
    		
    		leftList = list.subList(0, (list.size() - 1) / 2);
        	rightList = list.subList((list.size() - 1) / 2, list.size());
        	leftList = sort(leftList);
        	rightList = sort(rightList);

        	for (int i = 0; i < leftList.size(); i++) {
    			for (int j = 0; j < rightList.size(); j++) {
            		if ((int) leftList.get(i) <= (int) rightList.get(j)) {
						returnList.add(leftList.get(i));
						break;
					} else if (((int) leftList.get(i) > (int) rightList.get(j))) {
						returnList.add(rightList.get(j));
						rightList.remove(j);
					}
				}
    		}
        	
        	returnList.addAll(rightList);
        	
    	} else if (list.size() == 2) {

    		if ((int) list.get(0) <= (int) list.get(1)) {
				returnList.add(list.get(0));
				returnList.add(list.get(1));
    		} else {
    			returnList.add(list.get(1));
    			returnList.add(list.get(0));
    		}

    	} else if (list.size() == 1) {
    		returnList.add(list.get(0));
    	}

    	return returnList;    	
    } 
        
}