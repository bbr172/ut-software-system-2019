package ss.week4;

import java.util.ArrayList;
import java.util.List;

public class MergeSortTmp {
				
    public static <E extends Comparable<E>> void mergesort(List<E> list) {   	
//    	System.out.println("Originale " + list);

//    	MergeSort m = new MergeSort();
    	list = MergeSortTmp.sort(list);
    	System.out.println("Sorted " + list);

    }
    
    public static <E extends Comparable<E>> List<E> sort(List<E> list) {
    	List<E> sortedList = new ArrayList<E>();
    	
//    	System.out.println(list);
//    	if (list.size() > 1) {

    		List<E> leftList;
        	List<E> rightList;
    		
//        	System.out.println((list.size() -1 ) / 2 + 1);
    		leftList = list.subList(0, (list.size() - 1) / 2 + 1);
        	rightList = list.subList((list.size() - 1) / 2 + 1, list.size());
        	System.out.println("L: " + leftList + "   R: " + rightList);
        	if (rightList.size() > 1) {
        		leftList = sort(leftList);
        	}
        	if (leftList.size() > 1) {
        		rightList = sort(rightList);
        	}

        	for (int i = 0; i < leftList.size(); i++) {
    			for (int j = 0; j < rightList.size(); j++) {
            		if ((int) leftList.get(i) <= (int) rightList.get(j)) {
//            			System.out.println("L: " + leftList.get(i));
						sortedList.add(leftList.get(i));
						break;
					} else if (((int) leftList.get(i) > (int) rightList.get(j))) {
//            			System.out.println("R: " + rightList.get(i));
						sortedList.add(rightList.get(j));
						rightList.remove(j);
					}
				}
    		}
        	
        	
        	sortedList.addAll(rightList);
        	
//    	} else if (list.size() == 2) {
//
//    		if ((int) list.get(0) <= (int) list.get(1)) {
//				sortedList.add(list.get(0));
//				sortedList.add(list.get(1));
//    		} else {
//    			sortedList.add(list.get(1));
//    			sortedList.add(list.get(0));
//    		}
//
//    	} else {//if (list.size() == 1) {
//    		sortedList.add(list.get(0));
//    	}

    	return sortedList;    	
    } 
        
}
