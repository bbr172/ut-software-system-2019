package ss.week4.tictactoe;

import ss.utils.TextIO;

public class TicTacToe {

	public static void main(String[] args) {
		String p1;
		String p2;
		if (args.length == 2) {
			p1 = args[0];
			p2 = args[1];
		} else {
			System.out.println("Name player 1:");
			p1 = TextIO.getln();
			System.out.println("Name player 2:");
			p2 = TextIO.getln();
		}
		Game g = new Game(new HumanPlayer(p1, Mark.OO), new HumanPlayer(p2, Mark.XX));
		g.start();
	}

}
