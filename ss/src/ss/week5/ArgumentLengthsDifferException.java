package ss.week5;

public class ArgumentLengthsDifferException extends WrongArgumentException {

	private String afterDiffer;
	
	public ArgumentLengthsDifferException(String s1, String s2) {
		afterDiffer = "(" + s1.length() + ", " + s2.length() + ")";
	}

	public ArgumentLengthsDifferException(int i, int j) {
		afterDiffer = "(" + i + ", " + j + ")";
	}

	@Override
	public String getMessage() {
		return "length of command line arguments differ " + this.afterDiffer;
	}

}
