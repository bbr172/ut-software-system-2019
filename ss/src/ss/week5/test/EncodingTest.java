package ss.week5.test;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

/**
 * A simple class that experiments with the Hex encoding
 * of the Apache Commons Codec library.
 *
 */
public class EncodingTest {
    public static void main(String[] args) throws DecoderException {
        String input = "Hello World";
        input = "Hello Big World";
        String encodedString = Hex.encodeHexString(input.getBytes());
        
        System.out.println(encodedString);
        
        System.out.println(new String(Hex.decodeHex(encodedString)));

        System.out.println(Base64.encodeBase64String(input.getBytes()));
        
        encodedString = "010203040506";
        System.out.println(Base64.encodeBase64String(Hex.decodeHex(encodedString))); //Smaller (less memory)
        
        encodedString = "U29mdHdhcmUgU3lzdGVtcw==";
        System.out.println(new String(Base64.decodeBase64(encodedString)));
        
        String a = "a";
        for (int i = 0; i < 10; i++) {
        	System.out.println(a);
			System.out.println(Base64.encodeBase64String(a.getBytes()));
			a += "a";
		}
    }
}
