package ss.week5.tictactoe;

import ss.week4.tictactoe.*;

public class ComputerPlayer extends Player {
	
	private Strategy strategy;
	
	public ComputerPlayer (Mark mark, Strategy strategy) {
		super(strategy.getName() + " - " + mark.name(), mark);
		this.strategy = strategy;
	}
	
	public ComputerPlayer (Mark mark) {
		this(mark, new NaiveStrategy());
	}

	@Override
	public int determineMove(Board board) {
		return this.strategy.determineMove(board, this.getMark());
	}

}
