package ss.week5.tictactoe;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import ss.week4.tictactoe.*;


public class NaiveStrategy implements Strategy {

	@Override
	public int determineMove(Board board, Mark mark) {
		return randomMove(board);
	}

}
