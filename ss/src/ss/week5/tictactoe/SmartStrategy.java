package ss.week5.tictactoe;

import ss.week4.tictactoe.Board;
import ss.week4.tictactoe.Mark;

public class SmartStrategy implements Strategy{

	@Override
	public int determineMove(Board board, Mark mark) {
		if (board.isEmptyField(board.DIM/2, board.DIM/2)) {
			//Center
			return board.index(board.DIM/2, board.DIM/2);
		}
		
		int DirectWin = CheckWin(board, mark);
		if (DirectWin > -1) {
			return DirectWin;
		}
		
		int DirectBlock = CheckWin(board, (mark.equals(mark.OO) ? mark.XX : mark.OO));
		if (DirectBlock > -1) {
			return DirectBlock;
		}
		
		return randomMove(board);
	}
	
	public int CheckWin(Board board, Mark mark) {
		//Check for wining row
		for (int i = 0; i < board.DIM; i++) {
    		int rowCount = 0;
    		int rowEmpty = -1;
    		for (int j = 0; j < board.DIM; j++) {
				if (board.getField(i, j).equals(mark)) {
					rowCount++;
				} else if (board.isEmptyField(i, j)){
					rowEmpty = board.index(i, j);
				}
			}
    		if (rowCount == board.DIM - 1 && rowEmpty > -1 && board.isEmptyField(rowEmpty)) {
    			return rowEmpty;
    		}
    	}
		
		//Check winning col
		for (int i = 0; i < board.DIM; i++) {
    		int colCount = 0;
    		int colEmpty = -1;
    		for (int j = 0; j < board.DIM; j++) {
				if (board.getField(j, i).equals(mark)) {
					colCount++;
				} else if (board.isEmptyField(j, i)){
					colEmpty = board.index(j, i);
				}
			}
    		if (colCount == board.DIM - 1 && colEmpty > -1 && board.isEmptyField(colEmpty)) {
    			return colEmpty;
    		}
    	}
		
		//Check winning diagonal
//    	boolean fullDia = false;
    	int same = 0;
    	int diaEmpty = -1;
    	
    	for (int i = 0; i < board.DIM; i++) {
    		if (board.getField(i,i).equals(mark)) {
    			same++;
    		} else {
    			diaEmpty = board.index(i, i);
    		}
    		if (same == board.DIM - 1 && diaEmpty > -1 && board.isEmptyField(diaEmpty)) {
    			return diaEmpty;
    		}
    	}
    	
    	same = 0;
    	diaEmpty = -1;
    	for (int i = 0; i < board.DIM; i++) {
    		if (board.getField(i,board.DIM -1 -i).equals(mark)) {
    			same++;
    		} else {
    			diaEmpty = board.index(i, board.DIM -1 -i);
    		}
    		if (same == board.DIM - 1 && diaEmpty > -1 && board.isEmptyField(diaEmpty)) {
    			return diaEmpty;
    		}
    	}
		
		
//		int diaCount = 0;
//		int diaEmpty = -1;
//    	int[] index = {0, 4, 8, 2, 4, 6};
//    	for (int i = 0; i < index.length; i++) {
//    		if (index[i] == 2) {
//    			if (diaCount == board.DIM - 1 && diaEmpty > -1) {
//    				return diaEmpty;
//    			}
//    			diaCount = 0;
//    			diaEmpty = -1;
//    		}
//    		
//			if (board.getField(i).equals(mark)) {
//				diaCount++;
//			} else if (board.isEmptyField(i)){
//				diaEmpty = index[i];
//			}
//    	}
//		if (diaCount == board.DIM - 1 && diaEmpty > -1) {
//			return diaEmpty;
//		}
		
		//If not winning, return -1
		return -1;
	}

}
