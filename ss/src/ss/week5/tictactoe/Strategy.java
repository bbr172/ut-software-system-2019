package ss.week5.tictactoe;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import ss.week4.tictactoe.*;

public interface Strategy {

	public default String getName() {
		return this.getClass().getSimpleName();
	}
	
	public int determineMove(Board board, Mark mark);
	
	public default int randomMove(Board board) {
		Set<Integer> empty = new HashSet<Integer>();
		for (int i = 0; i < board.DIM * board.DIM; i++) {
			if (board.isEmptyField(i)) {
				empty.add(i);
			}
		}
		return (int) empty.toArray()[(new Random()).nextInt(empty.toArray().length)];
	}
}
