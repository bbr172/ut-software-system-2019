package ss.week5.tictactoe;

import ss.utils.TextIO;
import ss.week4.tictactoe.*;

public class TicTacToe {

	public static void main(String[] args) {
		Player p1;
		Player p2;
		if (args.length == 2) {
			p1 = dertimePlayer(args[0], Mark.OO);
			p2 = dertimePlayer(args[1], Mark.XX);
		} else {
			System.out.println("Name player 1:");
			p1 = dertimePlayer(TextIO.getln(), Mark.OO);
			System.out.println("Name player 2:");
			p2 = dertimePlayer(TextIO.getln(), Mark.XX);
		}
		
		Game g = new Game(p1, p2);
		g.start();
	}
	
	public static Player dertimePlayer(String s, Mark m) {
		Player p; 
		if (s.toLowerCase().equals("-n")) {
			p = new ComputerPlayer(m);
		} else if (s.toLowerCase().equals("-s")){
			p = new ComputerPlayer(m, new SmartStrategy());
		}else {
			p = new HumanPlayer(s, m);
		}
		return p;
	}

}
