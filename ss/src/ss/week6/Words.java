package ss.week6;

import java.util.Scanner;

public class Words {
	
	private static final String END_WORD = "end";

	public static void main(String[] args) {
		String input = "";
		
		while(!input.equals(END_WORD)) {		
			System.out.print("Line (or \"" + END_WORD + "\"): ");
			
			Scanner s = new Scanner(System.in);
			input = s.nextLine();
			
			if (input.equals(END_WORD)) break;
			
			String[] words = input.split(" ");
			
			for (int i = 0; i < words.length; i++) {
				System.out.println("Word " + (i+1) + ":" + words[i]);
			}
		}
		
		System.out.print("End of programme.");
		
	}
}
