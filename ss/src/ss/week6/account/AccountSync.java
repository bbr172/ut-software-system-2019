package ss.week6.account;

import java.util.concurrent.locks.ReentrantLock;

public class AccountSync {

	
	public static void main(String[] args) {
		for (int i = 0; i < 10000; i++) {
			test();
		}
	}
	
	public static void test() {
		Account a = new Account();
		int i = 15;
		Thread t1 = new Thread(new MyThread(12, i, a), "T1");
		Thread t2 = new Thread(new MyThread(-12, i, a), "T2");

		t1.start();
		t2.start();
		
		while (t1.isAlive() || t2.isAlive()) {}
		System.out.println(a.getBalance());
	}
}
