package ss.week6.dictionaryattack;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;

public class DictionaryAttack {
	private Map<String, String> passwordMap;
	private Map<String, String> hashDictionary;

	/**
	 * Reads a password file. Each line of the password file has the form:
	 * username: encodedpassword
	 * 
	 * After calling this method, the passwordMap class variable should be
	 * filled with the content of the file. The key for the map should be
	 * the username, and the password hash should be the content.
	 * @param filename
	 */
	public void readPasswords(String filename) {
		if (passwordMap == null) passwordMap = new HashMap<String, String>();
		try {
			Files.readAllLines(new File(filename).toPath()).forEach(line -> {
				String[] s = line.split(": ");
				if (s.length == 2) passwordMap.put(s[0], s[1]);	
			});
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * Given a password, return the MD5 hash of a password. The resulting
	 * hash (or sometimes called digest) should be hex-encoded in a String.
	 * @param password
	 * @return
	 */
	public String getPasswordHash(String password) {
		return DigestUtils.md5Hex(password);
	}
	/**
	 * Checks the password for the user the password list. If the user
	 * does not exist, returns false.
	 * @param user
	 * @param password
	 * @return whether the password for that user was correct.
	 */
	public boolean checkPassword(String user, String password) {
		return passwordMap != null && passwordMap.get(user).equals(getPasswordHash(password));
	}

	/**
	 * Reads a dictionary from file (one line per word) and use it to add
	 * entries to a dictionary that maps password hashes (hex-encoded) to
     * the original password.
	 * @param filename filename of the dictionary.
	 */
   	public void addToHashDictionary(String filename) {
   		if (hashDictionary == null) hashDictionary = new HashMap<String, String>();
		try {
			Files.readAllLines(new File(filename).toPath()).forEach(line -> {
				hashDictionary.put(getPasswordHash(line), line);
			});
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
   	}
	
   	/**
	 * Do the dictionary attack.
	 */
	public void doDictionaryAttack() {
		if (passwordMap != null & hashDictionary != null) {
			passwordMap.forEach((user, passH) -> {
				String pass = hashDictionary.get(passH);
				if (pass != null) {
					System.out.println(user + "\t\t" + pass);
				}
			});
		}
	}
	
	public static void main(String[] args) {
		DictionaryAttack da = new DictionaryAttack();
        da.readPasswords("./src/ss/week6/test/LeakedPasswords.txt");
        da.addToHashDictionary("./src/ss/week6/dictionaryattack/mostUsedPass");
		da.doDictionaryAttack();
	}

}
