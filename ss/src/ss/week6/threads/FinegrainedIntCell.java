package ss.week6.threads;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class FinegrainedIntCell implements IntCell {
	
	private int value = 0;
	private boolean isSet = false;
	private final ReentrantLock l = new ReentrantLock();
	private final Condition set = l.newCondition();
	private final Condition get = l.newCondition();
	
	public void setValue(int valueArg) {
		l.lock();
		try {
			while(isSet) {
				try {
					set.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} finally {
			this.value = valueArg;
			isSet = true;
			get.signal();
			l.unlock();
		}
	}

	public int getValue() {
		l.lock();
		try {
			while(!isSet) {
				try {
					get.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} finally {
			isSet = false;
			set.signal();
			l.unlock();
		}
		return value;
	}
	

}
