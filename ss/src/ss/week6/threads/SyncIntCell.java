package ss.week6.threads;

public class SyncIntCell implements IntCell {
	
	private int value = 0;
	private boolean set = false;
	
	public synchronized void setValue(int valueArg) {
		while(set) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		this.value = valueArg;
		set = true;
		notifyAll(); //all for all threads
	}

	public synchronized int getValue() {
		while(!set) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		set = false;
		notifyAll();
		
		return value;
	}

}
