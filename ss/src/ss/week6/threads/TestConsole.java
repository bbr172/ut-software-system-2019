package ss.week6.threads;

public class TestConsole implements Runnable {

	@Override
	public void run() {
		sum();
	}
	
	private void sum() {
		int i = Console.readInt(Thread.currentThread().getName() + ": get number 1? ");
		int i2 = Console.readInt(Thread.currentThread().getName() + ": get number 2? ");
		System.out.println(Thread.currentThread().getName() + ": " + i + " + " + i2 + " = " + (i+i2));
	}
	
	public static void main(String[] args) {
		new Thread(new TestConsole(), "Thread A").start();
		new Thread(new TestConsole(), "Thread B").start();
	}

}
