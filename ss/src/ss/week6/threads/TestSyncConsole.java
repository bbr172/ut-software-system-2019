package ss.week6.threads;

import java.util.concurrent.locks.ReentrantLock;

public class  TestSyncConsole implements Runnable {
	
	private final ReentrantLock l = new ReentrantLock();

	@Override
	public void run() {
//		sum();
		sum2();
	}
	
	private void sum() {
		synchronized (TestSyncConsole.class) {
			int i = SyncConsole.readInt(Thread.currentThread().getName() + ": get number 1? ");
			int i2 = SyncConsole.readInt(Thread.currentThread().getName() + ": get number 2? ");
			System.out.println(Thread.currentThread().getName() + ": " + i + " + " + i2 + " = " + (i+i2));		
		}
	}
	
	private void sum2() {
		l.lock();
		try {
			int i = SyncConsole.readInt(Thread.currentThread().getName() + ": get number 1? ");
			int i2 = SyncConsole.readInt(Thread.currentThread().getName() + ": get number 2? ");
			System.out.println(Thread.currentThread().getName() + ": " + i + " + " + i2 + " = " + (i+i2));
		}
		finally {
			l.unlock();
		}
	}
		
	public static void main(String[] args) {
		new Thread(new TestSyncConsole(), "Thread A").start();
		new Thread(new TestSyncConsole(), "Thread B").start();
	}

	//https://javarevisited.blogspot.com/2013/03/reentrantlock-example-in-java-synchronized-difference-vs-lock.html
}
