package ss.week7.hotel.client;

import java.net.InetAddress;
import java.util.Scanner;

import ss.utils.TextIO;
import ss.week3.bill.SysoutPrinter;
import ss.week3.hotel.PricedSafe;
import ss.week3.hotel.Room;
import ss.week7.hotel.exceptions.ExitProgram;
import ss.week7.hotel.exceptions.ServerUnavailableException;

public class HotelClientTUI implements HotelClientView {

	private HotelClient hc;
	private boolean input = true;
	
	public HotelClientTUI(HotelClient hcarg) {
		hc = hcarg;
	}
	
	@Override
	public void start() throws ServerUnavailableException {
		Scanner s = new Scanner(System.in);
		while(input) {
			try {
				handleUserInput(s.nextLine());
			} catch (ExitProgram e) {
				hc.sendExit();
				e.printStackTrace();
			} catch (ServerUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void handleUserInput(String input) throws ExitProgram, ServerUnavailableException {
		String split[] = input.split(" ");
		String command = split[0];
		String parm1 = null;
		String parm2 = null;
		if (split.length > 1 && split[1].replace(" ", "").length() > 0) {
			parm1 = split[1];
		}
		if (split.length > 2 && split[2].replace(" ", "").length() > 0) {
			parm2 = split[2];
		}
					
		switch(command) {
		case "i":
			hc.doIn(parm1);
			break;
		case "o": 
			hc.doOut(parm1);
			break;
		case "r": 
			hc.doRoom(parm1);
			break;
		case "a":
			hc.doAct(parm1, parm2);
			break;
		case "b":
			hc.doBill(parm1, parm2);
			break;
		case "h": 
			printHelpMenu();
			break;
		case "p":
			hc.doPrint();
			break;
		case "x": 
			this.input = false;
			hc.sendExit();
			break;
		default: 
			showMessage("Wrong input");
		}
	}

	@Override
	public void showMessage(String message) {
		System.out.println(message);
	}

	@Override
	public InetAddress getIp() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getString(String question) {
		return null;
	}

	@Override
	public int getInt(String question) {
		return 0;
	}

	@Override
	public boolean getBoolean(String question) {
		return true;
	}

	@Override
	public void printHelpMenu() {
		System.out.println("Welcome to the Hotel booking system of the " + hc.getHotelName() + "\n"
				+ "Commands:\n"
				+ "i name ............... check in guest with name\n"
				+ "o name ............... check out guest with name\n"
				+ "r name ............... request room of guest\n"
				+ "a name password ...... activate safe, password required for PricedSafe\n"
				+ "b name nights ........ print bill for guest (name) and number of nights\n"
				+ "h .................... printHelpMenu ( this menu )\n"
				+ "p .................... print state\n"
				+ "x .................... exit\n\n");
	}

}
