package ss.week7.hotel.server;

import java.io.IOException;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ss.week7.hotel.client.HotelClientTUI;
import ss.week7.hotel.exceptions.ExitProgram;
import ss.week7.hotel.protocol.ProtocolMessages;
import ss.week7.hotel.protocol.ServerProtocol;
import ss.week3.bill.StringPrinter;
import ss.week3.bill.SysoutPrinter;
import ss.week3.hotel.*;
/**
 * Server TUI for Networked Hotel Application
 * 
 * Intended Functionality: interactively set up & monitor a new server
 * 
 * @author Wim Kamerman
 */
public class HotelServer implements Runnable, ServerProtocol {

	/** The ServerSocket of this HotelServer */
	private ServerSocket ssock;

	/** List of HotelClientHandlers, one for each connected client */
	private List<HotelClientHandler> clients;
	
	/** Next client number, increasing for every new connection */
	private int next_client_no;

	/** The view of this HotelServer */
	private HotelServerTUI view;

	/** The name of the Hotel */
	private String HOTELNAME = "U Parkhotel";

	private Hotel hotel;
	
	/**
	 * Constructs a new HotelServer. Initializes the clients list, 
	 * the view and the next_client_no.
	 */
	public HotelServer() {
		clients = new ArrayList<>();
		view = new HotelServerTUI();
		next_client_no = 1;
	}
	
	/**
	 * Returns the name of the hotel
	 * 
	 * @requires hotel != null;
	 * @return the name of the hotel.
	 */
	public String getHotelName() {
		return HOTELNAME;
	}

	/**
	 * Opens a new socket by calling {@link #setup()} and starts a new
	 * HotelClientHandler for every connecting client.
	 * 
	 * If {@link #setup()} throws a ExitProgram exception, stop the program. 
	 * In case of any other errors, ask the user whether the setup should be 
	 * ran again to open a new socket.
	 */
	public void run() {
		boolean openNewSocket = true;
		while (openNewSocket) {
			try {
				// Sets up the hotel application
				setup();

				while (true) {
					Socket sock = ssock.accept();
					String name = "Client " 
							+ String.format("%02d", next_client_no++);
					view.showMessage("New client [" + name + "] connected!");
					HotelClientHandler handler = 
							new HotelClientHandler(sock, this, name);
					new Thread(handler).start();
					clients.add(handler);
				}

			} catch (ExitProgram e1) {
				// If setup() throws an ExitProgram exception, 
				// stop the program.
				openNewSocket = false;
			} catch (IOException e) {
				System.out.println("A server IO error occurred: " 
						+ e.getMessage());

				if (!view.getBoolean("Do you want to open a new socket?")) {
					openNewSocket = false;
				}
			}
		}
		view.showMessage("See you later!");
	}

	/**
	 * Sets up a new Hotel using {@link #setupHotel()} and opens a new 
	 * ServerSocket at localhost on a user-defined port.
	 * 
	 * The user is asked to input a port, after which a socket is attempted 
	 * to be opened. If the attempt succeeds, the method ends, If the 
	 * attempt fails, the user decides to try again, after which an 
	 * ExitProgram exception is thrown or a new port is entered.
	 * 
	 * @throws ExitProgram if a connection can not be created on the given 
	 *                     port and the user decides to exit the program.
	 * @ensures a serverSocket is opened.
	 */
	public void setup() throws ExitProgram {
		// First, initialize the Hotel.
		setupHotel();

		ssock = null;
		while (ssock == null) {
			int port = view.getInt("Please enter the server port.");

			// try to open a new ServerSocket
			try {
				view.showMessage("Attempting to open a socket at 127.0.0.1 "
						+ "on port " + port + "...");
				ssock = new ServerSocket(port, 0, 
						InetAddress.getByName("127.0.0.1"));
				view.showMessage("Server started at port " + port);
			} catch (IOException e) {
				view.showMessage("ERROR: could not create a socket on "
						+ "127.0.0.1" + " and port " + port + ".");

				if (!view.getBoolean("Do you want to try again?")) {
					throw new ExitProgram("User indicated to exit the "
							+ "program.");
				}
			}
		}
	}
	
	/**
	 * Asks the user for a hotel name and initializes
	 * a new Hotel with this name.
	 */
	public void setupHotel() {
		String s = view.getString("Name of hotel:");
		this.HOTELNAME = s;
		this.hotel = new Hotel(s);
	}
	
	/**
	 * Removes a clientHandler from the client list.
	 * @requires client != null
	 */
	public void removeClient(HotelClientHandler client) {
		this.clients.remove(client);
	}

	// ------------------ Server Methods --------------------------

	@Override
	public String getHello() {
		return ProtocolMessages.HELLO + ProtocolMessages.DELIMITER + HOTELNAME; 
	}
	
	@Override
	public synchronized String doIn(String cmd) {
		if (cmd == null) {
			return "Parameter is wrong";
		} 	else if(hotel.getRoom(cmd) != null){
			return "The guest alreay has a room";
		} else if(hotel.getFreeRoom() == null) {
			return "The hotel is full";
		} else {
			Room r = hotel.checkIn(cmd);
			if (r == null) {
				return "Somingt went wrong";
			} else {
				return "Guest " + cmd + " is assigned to room: " + r.getNumber();
			}
		}
	}

	@Override
	public synchronized String doOut(String cmd) {
		if(cmd == null) {
			return "Missing guest name";
		} else if(hotel.empty()) {
			return "No guest in hotel";
		} else if(hotel.getRoom(cmd) == null) {
			return "Guest " + cmd + " doesn't have a room";
		} else {
			hotel.checkOut(cmd);
			return "Guest " + cmd + " has checked out";
		}
	}

	@Override
	public synchronized String doRoom(String cmd) {
		if(cmd == null) {
			return "Missing guest name";
		} else if(hotel.empty()) {
			return "No guest in hotel";
		} else {
			Room r = hotel.getRoom(cmd);
			if (r == null) {
				return "Guest " + cmd + " doesn't have a room";
			} else {
				return "Guest " + cmd + " have room " + r.getNumber();
			}
		}
	}

	@Override
	public synchronized String doAct(String cmd1, String cmd2) {
		if(cmd1 == null) {
			return "Missing guest name";
		} else {
			Room r = hotel.getRoom(cmd1);
			if(r == null){
				return "The guest does not have a room";
			} else if (r.getSafe() == null) {
				return "No safe in room";
			} else if (r.getSafe() instanceof PricedSafe && cmd2 == null) {
				return "Missing password";
			} else if (r.getSafe() instanceof PricedSafe) {
				((PricedSafe) r.getSafe()).activate(cmd2);
				if (r.getSafe().isActive()) {
					return "Safe in " + r.getNumber() + " of guest " + cmd1 + " has been activated.";
				} else {
					return "Wrong password";
				}
			} else {
				r.getSafe().activate();
				return "Safe in " + r.getNumber() + " of guest " + cmd1 + " has been activated.";
			}
		}
	}

	@Override
	public synchronized String doBill(String cmd1, String cmd2) {
		if(cmd1 == null) {
			return "Missing guest name";
		} else if(hotel.getRoom(cmd1) == null) {
			return "Guest " + cmd1 + " doesn't have a room";
		} else if (cmd2 == null) {
			return "Missing nights";
		} else {
			try {
				String s = ((StringPrinter) hotel.getBill(cmd1, Integer.parseInt(cmd2), new StringPrinter()).getPrinter()).getResult();;
				return multipleLines(s);
			} catch (NumberFormatException e) {
				return "Second parameter is not a number";
			}
		}
	}
	
	private String multipleLines (String s) {
		return s + "\n" + ProtocolMessages.EOT;
	}

	@Override
	public synchronized String doPrint() {
		if(hotel.empty()) {
			return "No guest in hotel";
		} else {
			String s = hotel.toString();
			return multipleLines(s);
		}
	}

	// ------------------ Main --------------------------

	/** Start a new HotelServer */
	public static void main(String[] args) {
		HotelServer server = new HotelServer();
		System.out.println("Welcome to the Hotel Server! Starting...");
		new Thread(server).start();
	}
	
	private boolean randomBool() {
	    Random random = new Random();
	    return random.nextBoolean();
	}
	
}
