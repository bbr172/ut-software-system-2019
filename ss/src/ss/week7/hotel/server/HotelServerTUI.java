package ss.week7.hotel.server;

import java.io.PrintWriter;
import java.util.Scanner;


/**
 * Hotel Server TUI for user input and user messages
 * 
 * @author Wim Kamerman
 */
public class HotelServerTUI implements HotelServerView {
	
	/** The PrintWriter to write messages to */
	private PrintWriter console;

	/**
	 * Constructs a new HotelServerTUI. Initializes the console.
	 */
	public HotelServerTUI() {
		console = new PrintWriter(System.out, true);
	}

	@Override
	public void showMessage(String message) {
		console.println(message);
	}
	
	@Override
	public String getString(String question) {
		Scanner s = new Scanner(System.in);
		while (true) {
			System.out.println(question);
			String str = s.nextLine();
			if (str.length() > 0) {
				return str;
			}
		}
	}

	@Override
	public int getInt(String question) {
		Scanner s = new Scanner(System.in);
		while (true) {
			System.out.println(question);
			String str = s.nextLine();
			try {
				return Integer.parseInt(str);
			} catch (Exception e) {}				
		}
	}

	@Override
	public boolean getBoolean(String question) {
		Scanner s = new Scanner(System.in);
		while (true) {
			System.out.println(question);
			String str = s.nextLine();
			if (str.toLowerCase().equals("y")) {
				return true;
			} else if (str.toLowerCase().equals("n")) {
				return false;
			}
		}
	}

}
